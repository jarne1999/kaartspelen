﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoetjeMelken
{
    class Card
    {
        private string valueName;
        private string soort;
        private int value;

        public string Soort
        {
            get
            {
                return soort;
            }
        }

        public string ValueName
        {
            get
            {
                return valueName;
            }
        }

        public int Value
        {
            get
            {
                return value;
            }
        }

        public Card(string withValueName, string withSoort, int withValue)
        {
            this.valueName = withValueName;
            this.soort = withSoort;
            this.value = withValue;
        }

        public override string ToString()
        {
            return String.Format("{0} of {1}", valueName, soort);
        }
    }
}
